﻿using System;
using System.Runtime.InteropServices;
using PowerState.Library.LibraryWrapper;
using PowerState.Library.Models;

namespace PowerState.Library
{
    [ComVisible(true)]
    [Guid("C5D42D5D-EB07-4875-99D3-BF2A506C511D")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerStateManager : IPowerStateManager
    {
        private const uint STATUS_SUCCESS = 0;

        public Int64 GetLastSleepTime()
        {
            int outBufferSize = Marshal.SizeOf(typeof(ulong));
            IntPtr dataBuffer = Marshal.AllocCoTaskMem(outBufferSize);

            try
            {
                int retval = PowerStateLibrary.CallNtPowerInformation(
                    POWER_INFORMATION_LEVEL.LastSleepTime,
                    IntPtr.Zero,
                    0,
                    dataBuffer,
                    outBufferSize
                    );

                return Marshal.ReadInt64(dataBuffer);
            }
            finally
            {
                Marshal.FreeCoTaskMem(dataBuffer);
            }
        }

        public Int64 GetLastWakeTime()
        {
            int outBufferSize = Marshal.SizeOf(typeof(ulong));
            IntPtr dataBuffer = Marshal.AllocCoTaskMem(outBufferSize);

            try
            {
                int retval = PowerStateLibrary.CallNtPowerInformation(
                    POWER_INFORMATION_LEVEL.LastWakeTime,
                    IntPtr.Zero,
                    0,
                    dataBuffer,
                    outBufferSize
                    );

                return Marshal.ReadInt64(dataBuffer);
            }
            finally
            {
                Marshal.FreeCoTaskMem(dataBuffer);
            }
        }

        public string GetSystemBatteryState()
        {
            int bufferSize = Marshal.SizeOf(typeof(PowerStateLibrary.SYSTEM_BATTERY_STATE));
            IntPtr dataBuffer = Marshal.AllocCoTaskMem(bufferSize);
            try
            {
                int retval = PowerStateLibrary.CallNtPowerInformation(
                    POWER_INFORMATION_LEVEL.SystemPowerInformation,
                    IntPtr.Zero,
                    0,
                    dataBuffer,
                    bufferSize
                    );

                PowerStateLibrary.SYSTEM_BATTERY_STATE systemBatteryState = Marshal.PtrToStructure<PowerStateLibrary.SYSTEM_BATTERY_STATE>(dataBuffer);
                return systemBatteryState.ToString();
            }
            finally
            {
                Marshal.FreeCoTaskMem(dataBuffer);
            }
        }

        public string GetSystemPowerInformation()
        {
            int bufferSize = Marshal.SizeOf(typeof(PowerStateLibrary.SYSTEM_POWER_INFORMATION));
            IntPtr dataBuffer = Marshal.AllocCoTaskMem(bufferSize);

            try
            {
                int retval = PowerStateLibrary.CallNtPowerInformation(
                    POWER_INFORMATION_LEVEL.SystemPowerInformation,
                    IntPtr.Zero,
                    0,
                    dataBuffer,
                    bufferSize
                    );

                PowerStateLibrary.SYSTEM_POWER_INFORMATION systemPowerInformation = Marshal.PtrToStructure<PowerStateLibrary.SYSTEM_POWER_INFORMATION>(dataBuffer);
                return systemPowerInformation.ToString();
            }
            finally
            {
                Marshal.FreeCoTaskMem(dataBuffer);
            }
        }

        public bool ReserveHiberFile(bool reserve)
        {
            int bufferSize = Marshal.SizeOf(typeof(short));
            IntPtr pointerToIsHibernate = Marshal.AllocCoTaskMem(bufferSize);

            try
            {
                Marshal.WriteInt16(pointerToIsHibernate, 0, (short)(reserve ? 1 : 0));

                int retval = PowerStateLibrary.CallNtPowerInformation(
                    POWER_INFORMATION_LEVEL.SystemReserveHiberFile,
                    pointerToIsHibernate,
                    bufferSize,
                    IntPtr.Zero,
                    0);

                return retval == STATUS_SUCCESS;
            }
            finally
            {
                Marshal.FreeCoTaskMem(pointerToIsHibernate);
            }
        }

        public bool SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent)
        {
            return PowerStateLibrary.SetSuspendState(hibernate, forceCritical, disableWakeEvent);
        }
    }

}
