﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerState.Library;

namespace PowerState.UnitTest
{
    [TestClass]
    public class PowerStateManagerTests
    {
        [TestMethod]
        public void GetLastSleepTime()
        {
            var powerStateManager = new PowerStateManager();

            long result = powerStateManager.GetLastSleepTime();
            
            Console.WriteLine(result);
        }

        [TestMethod]
        public void GetLastWakeTime()
        {
            var powerStateManager = new PowerStateManager();

            long result = powerStateManager.GetLastWakeTime();

            Console.WriteLine(result);
        }

        [TestMethod]
        public void GetSystemBatteryState()
        {
            var powerStateManager = new PowerStateManager();

            var result = powerStateManager.GetSystemBatteryState();

            Assert.IsNotNull(result);
            Console.WriteLine(result);
        }

        [TestMethod]
        public void GetSystemPowerInformation_Valid()
        {
            var powerStateManager = new PowerStateManager();

            var result = powerStateManager.GetSystemPowerInformation();

            Assert.IsNotNull(result);
            Console.WriteLine(result);
        }

//        [TestMethod]
//        public void ReserveHiberFile()
//        {
//            var powerStateManager = new PowerStateManager();
//
//            var result1 = powerStateManager.ReserveHiberFile(true);
//            var result2 = powerStateManager.ReserveHiberFile(false);
//            
//            Assert.IsTrue(result1);
//            Assert.IsTrue(result2);
//        }

//        [TestMethod]
//        public void SetSuspendState()
//        {
//            var powerStateManager = new PowerStateManager();
//
//            var result = powerStateManager.SetSuspendState(true, false, false);
//            
//            Assert.IsTrue(result);
//        }
    }
}
