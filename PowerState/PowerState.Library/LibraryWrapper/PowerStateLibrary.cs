﻿using System;
using System.Runtime.InteropServices;
using PowerState.Library.Models;

namespace PowerState.Library.LibraryWrapper
{
    public class PowerStateLibrary
    {
        public struct SYSTEM_POWER_INFORMATION
        {
            public uint MaxIdlenessAllowed;
            public uint Idleness;
            public uint TimeRemaining;
            public byte CoolingMode;

            public override string ToString()
            {
                return string.Format(
                    "The idleness at which the system is considered idle and the idle time-out begins counting, expressed as a percentage. Dropping below this number causes the timer to be canceled : {0}\n" +
                    "The current idle level, expressed as a percentage: {1}\n" +
                    "The time remaining in the idle timer, in seconds: {2}\n" +
                    "The current system cooling mode: {3}\n (0 - The system is currently in Active cooling mode.\n1- The system does not support CPU throttling, or there is no thermal zone defined in the system.\n2-The system is currently in Passive cooling mode.)",
                    MaxIdlenessAllowed,
                    Idleness,
                    TimeRemaining,
                    CoolingMode);
            }
        };

        public struct SYSTEM_BATTERY_STATE
        {
            public byte AcOnLine;
            public byte BatteryPresent;
            public byte Charging;
            public byte Discharging;
            public byte spare1;
            public byte spare2;
            public byte spare3;
            public byte spare4;
            public UInt32 MaxCapacity;
            public UInt32 RemainingCapacity;
            public Int32 Rate;
            public UInt32 EstimatedTime;
            public UInt32 DefaultAlert1;
            public UInt32 DefaultAlert2;

            public override string ToString()
            {
                return String.Format("AcOnLine: {0}," +
                                     " BatteryPresent: {1}," +
                                     " Charging: {2}," +
                                     " Discharging: {3}," +
                                     " MaxCapacity: {4}," +
                                     " RemainingCapacity: {5}," +
                                     " Rate: {6}," +
                                     " EstimatedTime: {7}",
                    AcOnLine,
                    BatteryPresent,
                    Charging,
                    Discharging,
                    MaxCapacity,
                    RemainingCapacity,
                    Rate,
                    EstimatedTime);
            }
        }

        [DllImport("powrprof.dll")]
        public static extern int CallNtPowerInformation(
            POWER_INFORMATION_LEVEL informationLevel,
            IntPtr lpInputBuffe,
            int nInputBufferSize,
            IntPtr lpOutputBuffer,
            int nOutputBufferSize
            );

        [DllImport("powrprof.dll")]
        public static extern bool SetSuspendState(Boolean hibernate, Boolean forceCritical, Boolean disableWakeEvent);
    }
}
