using System;
using System.Runtime.InteropServices;

namespace PowerState.Library
{
    [ComVisible(true)]
    [Guid("A5CB2D8B-96AA-4281-A4BD-70EC873F94D7")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IPowerStateManager
    {
        Int64 GetLastSleepTime();

        Int64 GetLastWakeTime();
        
        string GetSystemBatteryState();

        string GetSystemPowerInformation();

        bool ReserveHiberFile(bool reserve);

        bool SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent);
    }
}