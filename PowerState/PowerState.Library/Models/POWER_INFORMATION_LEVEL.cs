namespace PowerState.Library.Models
{
    public enum POWER_INFORMATION_LEVEL
    {
        SystemBatteryState = 5,
        SystemReserveHiberFile = 10,
        SystemPowerInformation = 12,
        LastWakeTime = 14,
        LastSleepTime = 15
    }
}